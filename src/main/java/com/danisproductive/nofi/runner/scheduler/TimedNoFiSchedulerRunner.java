package com.danisproductive.nofi.runner.scheduler;


import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiScheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimedNoFiSchedulerRunner extends NoFiSchedulerRunner<TimedNoFiScheduler> {
    private final transient ScheduledExecutorService scheduledExecutor;

    public TimedNoFiSchedulerRunner(TimedNoFiScheduler scheduler) {
        super(scheduler);
        scheduledExecutor = Executors.newScheduledThreadPool(super.schedulerConfig.getConcurrentTasks());
    }

    @Override
    public void schedule(Runnable processRunner) {
        scheduledExecutor.scheduleAtFixedRate(
                processRunner,
                0,
                super.schedulerConfig.getIntervalInMilliseconds(),
                TimeUnit.MILLISECONDS
        );
    }

    @Override
    public void stopSchedule() {
        scheduledExecutor.shutdown();

    }
}
