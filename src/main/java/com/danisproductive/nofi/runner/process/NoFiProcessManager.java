package com.danisproductive.nofi.runner.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.utils.config.FileConfigWriter;
import com.danisproductive.nofi.runner.json.RunnerJsonConverter;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class NoFiProcessManager {
    public static final String DATA_FILE_PATH = "nofi-manager-data.json";

    private Map<String, NoFiProcessRunner> processRunners;
    private final transient FileConfigWriter<NoFiProcessManager> configWriter;

    public NoFiProcessManager() {
        processRunners = new HashMap<>();
        configWriter = new FileConfigWriter<>(DATA_FILE_PATH, RunnerJsonConverter.gson);
    }

    public <T extends NoFiProcess> void addProcess(T process) {
        if (!processRunners.containsKey(process.getId())) {
            if (NoFiProcessRunnerFactory.containsKey(process.getClass())) {
                NoFiProcessRunner processRunner = NoFiProcessRunnerFactory.createProcessRunner(process);
                processRunners.put(process.getId(), processRunner);
            }
        }
    }

    public void removeProcess(String processId) {
        processRunners.remove(processId);
    }

    public <T extends NoFiProcess> NoFiProcessRunner getProcessRunner(String processId) {
        return processRunners.get(processId);
    }

    public void save() throws FileNotFoundException {
        configWriter.save(this);
    }
}
