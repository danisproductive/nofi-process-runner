package com.danisproductive.nofi.runner.server;

import com.danisproductive.nofi.runner.server.websocket.NoFiProcessRunnerConnectorListener;

public class MainServer {
    public static void main(String[] args) {
        new NoFiProcessRunnerConnectorListener().start();

        try {
            Object lock = new Object();
            synchronized (lock) {
                while (true) {
                    lock.wait();
                }
            }
        } catch (InterruptedException ex) {
        }
    }
}
