package com.danisproductive.nofi.runner.process.java;


import com.danisproductive.nofi.core.process.JavaNoFiProcess;
import com.danisproductive.nofi.runner.process.NoFiProcessRunner;

public class JavaNoFiProcessRunner extends NoFiProcessRunner<JavaNoFiProcess> {
    @Override
    protected String getCommandToRun() {
        processConfig.getCustomProcessorClasspath();

        return "java -jar ";
    }
}
