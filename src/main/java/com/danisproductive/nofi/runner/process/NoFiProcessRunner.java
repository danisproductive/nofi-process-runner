package com.danisproductive.nofi.runner.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.runner.scheduler.NoFiSchedulerRunner;
import com.danisproductive.nofi.runner.scheduler.NoFiSchedulerRunnerFactory;

public abstract class NoFiProcessRunner <T extends NoFiProcess> {
    protected T processConfig;
    private NoFiSchedulerRunner schedulerRunner;

    public NoFiProcessRunner(T processConfig) {
        this.processConfig = processConfig;
        schedulerRunner = NoFiSchedulerRunnerFactory.getSchedulerRunner(processConfig.getScheduler());
    }

    public NoFiSchedulerRunner getSchedulerRunner() {
        return schedulerRunner;
    }

    public void start() {
        if (!processConfig.isRunning()) {
            schedulerRunner.start(getCommandToRun());
        }
    }

    public void stop() {
        if (!processConfig.isRunning()) {
            schedulerRunner.stop();
        }
    }

    public T getProcessConfig() {
        return processConfig;
    }

    protected abstract String getCommandToRun();
}
