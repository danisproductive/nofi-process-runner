package com.danisproductive.nofi.runner.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConcurrentCommandRunner {
    private int concurrentTasks;
    private String command;
    private final transient Runtime runTime;
    private List<Process> externalProcess;

    public ConcurrentCommandRunner(int concurrentTasks) {
        this.concurrentTasks = concurrentTasks;
        runTime = Runtime.getRuntime();
        externalProcess = new ArrayList<>();
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public boolean start() {
        for (int index = 0; index < concurrentTasks; index++) {
            try {
                externalProcess.add(runTime.exec(command));
            } catch (IOException e) {
                e.printStackTrace();

                return false;
            }
        }

        return true;
    }

    public void stop() {
        externalProcess.forEach(Process::destroy);
    }
}
