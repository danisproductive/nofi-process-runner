package com.danisproductive.nofi.runner.json;

import com.google.gson.Gson;

public class RunnerJsonConverter {
    public static final Gson gson = new NoFiRunnerGsonBuilder().build();
}
