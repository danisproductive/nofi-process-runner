package com.danisproductive.nofi.runner.json;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.utils.json.NoFiCoreGsonBuilder;
import com.danisproductive.nofi.core.utils.json.adapter.DefaultHierarchyTypeAdapter;
import com.danisproductive.nofi.core.utils.json.adapter.process.NoFiProcessHierarchyTypeAdapter;
import com.danisproductive.nofi.runner.process.NoFiProcessRunner;
import com.google.gson.GsonBuilder;

public class NoFiRunnerGsonBuilder extends NoFiCoreGsonBuilder {
    @Override
    protected void registerAdapters(GsonBuilder gsonBuilder) {
        super.registerAdapters(gsonBuilder);
        gsonBuilder.registerTypeHierarchyAdapter(
                NoFiProcessRunner.class,
                new NoFiProcessRunnerTypeAdapter()
        );
    }
}
