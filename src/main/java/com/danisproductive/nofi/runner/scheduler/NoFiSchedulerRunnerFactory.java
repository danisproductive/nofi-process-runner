package com.danisproductive.nofi.runner.scheduler;

import com.danisproductive.nofi.core.scheduler.NoFiScheduler;
import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiScheduler;
import com.danisproductive.nofi.core.utils.MultiClassFactoryMap;


public class NoFiSchedulerRunnerFactory {
    private static final MultiClassFactoryMap<NoFiScheduler, NoFiSchedulerRunner> schedulerRunnerFactoryMap = new MultiClassFactoryMap<>();

    static {
        schedulerRunnerFactoryMap.put(TimedNoFiScheduler.class, TimedNoFiSchedulerRunner::new);
    }

    public static <T extends NoFiScheduler> NoFiSchedulerRunner getSchedulerRunner(T scheduler) {
        return schedulerRunnerFactoryMap.create(scheduler);
    }

    public static boolean containsKey(Class<? extends NoFiScheduler> processClass) {
        return schedulerRunnerFactoryMap.containsKey(processClass);
    }
}
