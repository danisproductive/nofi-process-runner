package com.danisproductive.nofi.runner.json;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.utils.json.adapter.process.NoFiProcessHierarchyTypeAdapter;
import com.danisproductive.nofi.runner.process.NoFiProcessRunner;
import com.danisproductive.nofi.runner.scheduler.NoFiSchedulerRunner;
import com.google.gson.*;

import java.lang.reflect.Type;

public class NoFiProcessRunnerTypeAdapter implements JsonSerializer<NoFiProcessRunner<NoFiProcess>>, JsonDeserializer<NoFiProcessRunner<NoFiProcess>> {
    public static final String CLASS_TYPE_PROPERTY = "type";

    private final Gson processGson = new GsonBuilder()
            .registerTypeHierarchyAdapter(
                    NoFiProcess.class,
                    new NoFiProcessHierarchyTypeAdapter()
            ).registerTypeHierarchyAdapter(
                    NoFiSchedulerRunner.class,
                    new NoFiSchedulerRunnerTypeAdapter()
            ).create();

    @Override
    public NoFiProcessRunner deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String className = json.getAsJsonObject().get(CLASS_TYPE_PROPERTY).getAsString();
        try {
            return processGson.fromJson(json, (Type) Class.forName(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public JsonElement serialize(NoFiProcessRunner src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject serializedObject = processGson.toJsonTree(src).getAsJsonObject();
        serializedObject.add(CLASS_TYPE_PROPERTY, new JsonPrimitive(src.getClass().getCanonicalName()));

        return serializedObject;
    }
}
