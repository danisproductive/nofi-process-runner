package com.danisproductive.nofi.runner.scheduler;

import com.danisproductive.nofi.core.scheduler.NoFiScheduler;
import com.danisproductive.nofi.runner.process.ConcurrentCommandRunner;


public abstract class NoFiSchedulerRunner<T extends NoFiScheduler> {
    protected T schedulerConfig;
    private ConcurrentCommandRunner commandRunner;

    public NoFiSchedulerRunner(T schedulerConfig) {
        this.schedulerConfig = schedulerConfig;
        commandRunner = new ConcurrentCommandRunner(schedulerConfig.getConcurrentTasks());
    }

    public T getSchedulerConfig() {
        return schedulerConfig;
    }

    public void setSchedulerConfig(T schedulerConfig) {
        this.schedulerConfig = schedulerConfig;
    }

    public void start(String command) {
        commandRunner.setCommand(command);
        schedule(() -> commandRunner.start());
    }

    public void stop() {
        commandRunner.stop();
        stopSchedule();
    }

    public abstract void schedule(Runnable processExecutor);

    public abstract void stopSchedule();
}
