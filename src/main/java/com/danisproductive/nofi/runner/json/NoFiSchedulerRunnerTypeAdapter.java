package com.danisproductive.nofi.runner.json;

import com.danisproductive.nofi.core.scheduler.NoFiScheduler;
import com.danisproductive.nofi.core.utils.json.adapter.DefaultHierarchyTypeAdapter;
import com.danisproductive.nofi.runner.scheduler.NoFiSchedulerRunner;
import com.google.gson.*;

import java.lang.reflect.Type;

public class NoFiSchedulerRunnerTypeAdapter implements JsonSerializer<NoFiSchedulerRunner<NoFiScheduler>>, JsonDeserializer<NoFiSchedulerRunner<NoFiScheduler>> {
    public static final String CLASS_TYPE_PROPERTY = "type";

    private final Gson processGson = new GsonBuilder()
            .registerTypeHierarchyAdapter(
                    NoFiScheduler.class,
                    new DefaultHierarchyTypeAdapter<>()
            ).create();

    @Override
    public NoFiSchedulerRunner<NoFiScheduler> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String className = json.getAsJsonObject().get(CLASS_TYPE_PROPERTY).getAsString();
        try {
            return processGson.fromJson(json, (Type) Class.forName(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public JsonElement serialize(NoFiSchedulerRunner src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject serializedObject = processGson.toJsonTree(src).getAsJsonObject();
        serializedObject.add(CLASS_TYPE_PROPERTY, new JsonPrimitive(src.getClass().getCanonicalName()));

        return serializedObject;
    }
}
