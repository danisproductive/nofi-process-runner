package com.danisproductive.nofi.runner.server.websocket;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.process.NoFiProcessUpdater;
import com.danisproductive.nofi.core.process.NoFiProcessValidator;
import com.danisproductive.nofi.core.server.websocket.NoFiWebSocketClient;
import com.danisproductive.nofi.core.utils.ValidationResult;
import com.danisproductive.nofi.core.utils.config.FileConfigReader;
import com.danisproductive.nofi.runner.json.RunnerJsonConverter;
import com.danisproductive.nofi.runner.process.NoFiProcessManager;
import com.danisproductive.nofi.runner.process.NoFiProcessRunner;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;

import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageProperty.*;
import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageType.*;
import static com.danisproductive.nofi.runner.process.NoFiProcessManager.DATA_FILE_PATH;

public class NoFiProcessRunnerConnectorListener {
    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_PROCESS_ADDED =
            (processManager, processJson) -> {
                JsonObject processConfigJson = processJson.get(PROCESS_CONFIG.getValue()).getAsJsonObject();
                ValidationResult processValidation = NoFiProcessValidator.validate(processConfigJson);

                if (processValidation.isValid()) {
                    NoFiProcess process = RunnerJsonConverter.gson.fromJson(processConfigJson, NoFiProcess.class);
                    processManager.addProcess(process);
                }
            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_PROCESS_UPDATED =
            (processManager, processJson) -> {
                JsonObject processConfigJson = processJson.get(PROCESS_CONFIG.getValue()).getAsJsonObject();
                ValidationResult validation = NoFiProcessValidator.validate(processConfigJson);

                if (validation.isValid()) {
                    NoFiProcessRunner processRunner = processManager.getProcessRunner(
                            processConfigJson.get(PROCESS_ID.getValue()).getAsString());

                    if (processRunner != null) {
                        NoFiProcess process = processRunner.getProcessConfig();
                        NoFiProcessUpdater.updateProcessFields(process, processConfigJson);

                        if (!process.getScheduler().equals(processRunner.getSchedulerRunner().getSchedulerConfig())) {
                            processRunner.getSchedulerRunner().setSchedulerConfig(process.getScheduler());
                        }
                    }

                }
            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_OUTPUTS_DELETED =
            (processManager, processJson) -> {
                JsonArray deletedOutputs = processJson.get(OUTPUTS_DELETED.getValue()).getAsJsonArray();

            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_INPUT_ADDED =
            (processManager, processJson) -> {

            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_PROCESS_CONNECTED =
            (processManager, processJson) -> {

            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_PROCESS_DISCONNECTED =
            (processManager, processJson) -> {

            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_PROCESS_STARTED =
            (processManager, processJson) -> {

            };

    private static final NoFiWebSocketClient.NoFiMessageProcessor<NoFiProcessManager> ON_PROCESS_STOPPED =
            (processManager, processJson) -> {

            };

    private NoFiWebSocketClient<NoFiProcessManager> listener;
    private final NoFiProcessManager processManager;

    public NoFiProcessRunnerConnectorListener() {
        NoFiProcessManager processManagerTmp;

        try {
            processManagerTmp = new FileConfigReader<NoFiProcessManager>(RunnerJsonConverter.gson)
                    .read(DATA_FILE_PATH, NoFiProcessManager.class);
        } catch (FileNotFoundException e) {
            processManagerTmp = new NoFiProcessManager();
        }

        processManager = processManagerTmp;
        try {
            listener = new NoFiWebSocketClient<>(processManager);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        listener.addMessageProcessor(PROCESS_ADDED.getValue(), ON_PROCESS_ADDED);
        listener.addMessageProcessor(PROCESS_UPDATED.getValue(), ON_PROCESS_UPDATED);
        listener.addMessageProcessor(OUTPUTS_DELETED.getValue(), ON_OUTPUTS_DELETED);
        listener.addMessageProcessor(INPUT_ADDED.getValue(), ON_INPUT_ADDED);
        listener.addMessageProcessor(PROCESS_CONNECTED.getValue(), ON_PROCESS_CONNECTED);
        listener.addMessageProcessor(PROCESS_DISCONNECTED.getValue(), ON_PROCESS_DISCONNECTED);
        listener.addMessageProcessor(PROCESS_STARTED.getValue(), ON_PROCESS_STARTED);
        listener.addMessageProcessor(PROCESS_STOPPED.getValue(), ON_PROCESS_STOPPED);

        listener.setAfterCallback(message -> {
            try {
                processManager.save();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public void start() {
        listener.connect();
    }
}
