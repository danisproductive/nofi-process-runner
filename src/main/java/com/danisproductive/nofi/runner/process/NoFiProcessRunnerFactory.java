package com.danisproductive.nofi.runner.process;

import com.danisproductive.nofi.core.process.JavaNoFiProcess;
import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.utils.MultiClassFactoryMap;
import com.danisproductive.nofi.runner.process.java.JavaNoFiProcessRunner;


public class NoFiProcessRunnerFactory {
    private static final MultiClassFactoryMap<NoFiProcess, NoFiProcessRunner> processRunnerFactoryMap = new MultiClassFactoryMap<>();

    static {
        processRunnerFactoryMap.put(JavaNoFiProcess.class, key -> new JavaNoFiProcessRunner());
    }

    public static <T extends NoFiProcess> NoFiProcessRunner createProcessRunner(T process) {
        return processRunnerFactoryMap.create(process);
    }

    public static boolean containsKey(Class<? extends NoFiProcess> processClass) {
        return processRunnerFactoryMap.containsKey(processClass);
    }
}
